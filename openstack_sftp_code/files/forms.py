from django import forms

class FileUploadForm(forms.Form):
    uploadedfile = forms.FileField()
