from django.conf.urls import patterns, url

from openstack_dashboard.dashboards.mosler.files import views # Does this work?


urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^(.*?)$', views.index_path, name='index_path'),
)


#from .views import IndexView
#
#urlpatterns = patterns('',
#    url(r'^$', IndexView.as_view(), name='index'),
#)
