from django.shortcuts import render


def index(request):
    """If no sub-url to the SFTP is given in the URI"""
    return index_path(request, '')

def index_path(request, path):
    """If there's a sub-url to the SFTP in the URI"""
    return render(request, "mosler/files/index.html", {
        'base': path,
        'exporter': request.user.has_perms( ('openstack.roles.exporter',) )
    })
