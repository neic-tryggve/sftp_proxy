from django.utils.translation import ugettext_lazy as _

import horizon

from openstack_dashboard.dashboards.mosler import dashboard


class Files(horizon.Panel):
    name = _("Import/Export")
    slug = "files"


dashboard.MoslerDashboard.register(Files)
