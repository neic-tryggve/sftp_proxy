from django.core.files.uploadhandler import FileUploadHandler, StopUpload
from django.core.files.uploadedfile import UploadedFile

import logging
LOG = logging.getLogger(__name__)


class SFTPUploadedFile(UploadedFile):
    """
    Django class for an uploaded file to the SFTP

    Added the sftp_client attribute over djagnos UploadedFile. Stores a handle
    to the server so we can unlink the file in case anything breaks.
    """
    def __init__(self, sftp_client = None, *args, **kwargs):
        super(SFTPUploadedFile, self).__init__(*args, **kwargs)
        self.sftp_client = sftp_client
        self.done = False

    def close(self):
        self.file.close()
        if self.done:
            return
        LOG.warning("Something went wrong with the upload, removing it on the sftp server")
        self.sftp_client.unlink(self.name)


class SFTPUploadHandler(FileUploadHandler):
    """
    Handler for uploading a file directly to the SFTP server without caching
    it on the local filesystem
    """
    chunk_size = 2 ** 20 # 1Mb chunks
    def __init__(self, project = None, sftp_client = None, workaround_client = None, *args, **kwargs):
        super(SFTPUploadHandler, self).__init__(*args, **kwargs)
        self.sftp_client = sftp_client
        self._workaround_client = workaround_client
        self.project = project

    def receive_data_chunk(self, raw_data, start):
        LOG.debug("Writing a chunk")
        try:
            self.file.write(raw_data)
        except IOError as e:
            LOG.warning("Could not write to the other end")
            self.file.close()
            raise StopUpload()
        except Error as e:
            LOG.error("Could not write to the other end: %s" % e)
            raise StopUpload()

        return None

    def recieve_data_chunk_fail(self, raw_data, start):
        LOG.warning("Recieve data chunk fail")
        raise StopUpload()

    def file_complete(self, file_size):
        return self.file

    def new_file(self, *args, **kwargs):
        super(SFTPUploadHandler, self).new_file(*args, **kwargs)

        try:
            self.sftp_client.chdir("/%s" % self.project)
        except StopUpload:
            LOG.warning("Halting upload")
            self.receive_data_chunk = self.recieve_data_chunk_fail
            return

        try:
            fh = self.sftp_client.open(self.file_name, mode='w', bufsize=-1)
        except IOError:
            LOG.warning("Could not open file %s in project %s for user %s" % (self.file_name, username, project))
            self.receive_data_chunk = self.recieve_data_chunk_fail
            return

        self.file = SFTPUploadedFile(file = fh, name = self.file_name, sftp_client=self.sftp_client)
