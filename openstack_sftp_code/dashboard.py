from django.utils.translation import ugettext_lazy as _

import horizon


class MoslerDashboard(horizon.Dashboard):
    name = _("Project")
    slug = "mosler"
    panels = ('overview', 'files',)  # Add your panels here.
    default_panel = 'overview'  # Specify the slug of the dashboard's default panel.
    urls = 'openstack_dashboard.dashboards.mosler.api_urls'


horizon.register(MoslerDashboard)
