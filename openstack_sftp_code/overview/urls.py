from django.conf.urls import patterns  # noqa
from django.conf.urls import url  # noqa

#import views
from .views import request_thinlinc, thinlinc, ProjectOverview, osx_swedish_keyboard, simple_view


urlpatterns = patterns('',
    #url(r'^$', ProjectOverview.as_view(), name='index'),
    url(r'^$', simple_view, name='index'),
    url(r'^request_thinlinc$', request_thinlinc),
    url(r'^thinlinc$', thinlinc, name='thinlinc'),
    url(r'^Swedish-nodeadkeys.keylayout$', osx_swedish_keyboard),
)
