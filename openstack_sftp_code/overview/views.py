import logging

from django.shortcuts import render
from django.http import HttpResponse
from django.core.urlresolvers import reverse
#from django.utils.translation import ugettext_lazy as _

import urllib2, urllib

#from horizon import views
from horizon import exceptions
#from openstack_dashboard.dashboards.project.instances \
#    import console as project_console

from openstack_dashboard import api
from openstack_dashboard import usage


LOG = logging.getLogger(__name__)

def request_thinlinc(request):    
    user = request.user
    LOG.debug("THE TENANT IS: %s" % user.tenant_name)
    token = user.token.id
    url = "http://thinlinc-master/project?token=%s&set=%s" % (token, user.tenant_name)
    LOG.debug("THINLINC REQUEST FOR <%s>" % url )
    try:
        handler = urllib2.urlopen(url)
    except urllib2.URLError as e:
        if e.getcode() == 401:
            return HttpResponse("Not authorized", status=401, content_type='text/plain')
        if e.getcode() == 500:
            return HttpResponse("Can't find project", status=404, content_type='text/plain')
	return HttpResponse("Something strange happened(%s)" % e.getcode(), status=500, content_type='text/plain')
    
    LOG.debug("THINLINC REQUEST GOT RESPONSE: %s", handler.getcode())
    if handler.getcode() == 200:
        url_to_return = reverse("horizon:mosler:overview:thinlinc")
        LOG.debug("THINLINC RETURNING URL: %s" % url_to_return)
        return HttpResponse(url_to_return, content_type='text/plain')
    return HttpResponse("Something strange happened2 (%s)" % handler.getcode(), status=500, content_type='text/plain')

def thinlinc(request):
    return render(request, 'mosler/overview/thinlinc.html')

def osx_swedish_keyboard(request):
    file = open("/usr/share/openstack-dashboard/static/dashboard/files/Swedish-nodeadkeys.keylayout", "r")
    data = file.read()
    response = HttpResponse(data, status=200, content_type='text/xml')
    response['Content-Disposition'] = 'attachment; filename="Swedish-nodeadkeys.keylayout"'
    return response

def simple_view(request):
    return render(request,'mosler/overview/overview.html')

class ProjectOverview(usage.UsageView):
    table_class = usage.ProjectUsageTable
    usage_class = usage.ProjectUsage
    template_name = 'mosler/overview/overview.html'
    #csv_response_class = ProjectUsageCsvRenderer

    def get_data(self):
        super(ProjectOverview, self).get_data()
        return self.usage.get_instances()

# vim: ts=4:sw=4:expandtab
