import os.path
import json
import stat
from math import log

from django.shortcuts import render
from django.http import (HttpResponse, HttpResponseNotAllowed,
			 HttpResponseNotFound, HttpResponseServerError)
from django.views.decorators.csrf import csrf_exempt, csrf_protect

import logging
LOG = logging.getLogger(__name__)


from .sftp import get_client
from .upload_handler import SFTPUploadHandler


def _get_info(request, type):
    if type == 'user':
        return request.user.username
    if type == 'pass':
        return request.user.token.id
    if type == 'project':
        return request.user.tenant_name
    if type == 'login':
        return (request.user.username, request.user.token.id)
    raise Error()

def mosler_js(request):
    return render(request, 'mosler/mosler.js', content_type='application/javascript')


@csrf_exempt
def put_files(request):
    """
    This is the AJAX frontend to fileuploading, has to be CSRF exempt so we can
    add the UploadHandler
    """
    LOG.debug("Registereing SFTPUploadHandler")

    try:
        sftp_client, workaround_client = get_client(*_get_info(request, 'login'))
    except:
        return HttpResponseServerError(json.dumps({'error': 'Could not upload the file'}), content_type='application/json')
    #request.upload_handlers = [FakeHandler(request)] # Only for devel
    project = _get_info(request, 'project')
    request.upload_handlers = [SFTPUploadHandler(project, sftp_client, workaround_client, request)]

    if request.method != 'POST':
        return HttpResponseNotAllowed(['POST'])

    return _put_files(request)


@csrf_protect
def _put_files(request):
    """Other half of the AJAX frontend, here we make sure the csrf cookie is validated"""
    LOG.debug("_put_files")
    # This triggers the download
    if not request.FILES.has_key('files[]'):
        LOG.debug("No files")
        return HttpResponse(json.dumps({'error': 'Could not upload the file'}), content_type='application/json', status=500)

    f = request.FILES['files[]']
    # If we don't mark it as done, the file will be removed from the server,
    # this is because the CSRF check happens after the UploadHandlers are run.
    LOG.debug("Setting done to True")
    f.done = True

    return HttpResponse(json.dumps({'files': [{'name':f.name}]}), content_type='application/json')


def list_files(request, path):
    """Method for listing files on the SFTP server that can be downloaded"""
    try:
        sftp_client, workaround_client = get_client(*_get_info(request, 'login'))
    except:
        LOG.error("Can't log into SFTP")
        return HttpResponseServerError()

    project = _get_info(request,'project')

    try:
        sftp_client.chdir("/OUTGOING/%s" % project)
    except IOError:
        username = _get_info(request, 'user')
        LOG.error("Could not change to project %s for user %s" % (project, username))
        return HttpResponseNotFound("Could not find any files on the server", content_type='text/plain')

    try:
        filelist = sftp_client.listdir_attr(path)
    except IOError:
        return HttpResponseNotFound("Can't find directory", content_type='text/plain')

    files = []
    for f in filelist:
        name = f.filename
        ftype = 'file'
        if stat.S_ISDIR(f.st_mode):
            ftype = 'dir'
        size, unit = _format_size(f.st_size)
        files.append({'name': name, 'type': ftype, 'size': size, 'unit': unit})

    string = json.dumps(sorted(files, key=lambda f: f["type"]))
    return HttpResponse(string, content_type='application/json')


class FileIterator(object):
    """An iterator over a file, needed for the HttpResponse in get_file()"""
    def __init__(self, fh, client):
        self.read_size = 2**20 # 1Mb
        self.fh = fh
        self.client = client

    def __iter__(self):
        return self

    def next(self):
        string = self.fh.read(self.read_size)
        LOG.debug("Reading one Mb, got: %d" % len(string))
        if len(string) > 0:
            return string
        else:
            raise StopIteration()


def get_file(request, filepath):
    """Download a file from the SFTP"""
    LOG.debug("Trying to download file at: <%s>" % (filepath))
    try:
        sftp_client, workaround_client = get_client(*_get_info(request, 'login'))
    except:
        return HttpResponseServerError()

    project = _get_info(request, 'project')

    try:
        sftp_client.chdir("/OUTGOING/%s" % project)
    except IOError:
        LOG.error("Could not change to project %s for user %s" % (project, username))
        return HttpResponseServerError()

    try:
        fh = sftp_client.file(filepath, mode='r')
        response = HttpResponse(FileIterator(fh, workaround_client))
        (dname, fname) = os.path.split( filepath )
        response['Content-Disposition'] = 'attachment; filename="%s"' % fname
        del response['Content-Type']
        return response
    except IOError:
        return HttpResponseNotFound()


def _format_size(size_in_bytes):
    """Makes formatting of file-sizes pretty"""
    units = ['b','kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb'] # Yes, we are future proof!
    unit = int( log(size_in_bytes) / log(1024) )
    size = size_in_bytes / 1024**unit
    return (size, units[unit])
