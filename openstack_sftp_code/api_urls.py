from django.conf.urls import patterns, url

from openstack_dashboard.dashboards.mosler import api_views


urlpatterns = patterns('',
    url(r'^api/list/*(.*)$', api_views.list_files, name='list_files'),
    url(r'^api/get/(.*)$',   api_views.get_file,   name='get_file'),
    url(r'^api/put$',        api_views.put_files,  name='put_files'),
    url(r'^mosler.js$',      api_views.mosler_js,  name='moslerjs'),
)
