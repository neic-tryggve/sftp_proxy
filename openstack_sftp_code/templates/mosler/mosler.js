{% load url from future %}
/* Additional JavaScript for mydashboard. */

/* THINLINC STUFF */

setup_thinlinc = function() {
	horizon.modals.modal_spinner("Preparing Log-in Node");
	aj = $.ajax({
		url: "request_thinlinc",
	}).done( function(data, textStatus, obj) {
		setTimeout( function() {
			window.location.href = data;
		}, 2000);
	}).fail( function(data, textStatus, obj) {
		horizon.modals.spinner.hide();
		$(".modal-backdrop").remove();
		horizon.alert("error", data.responseText);
	});
}


/* EXPORT IMPORT STUFF */

$(".mosler-choose-files").click(function() { $("#mosler-fileupload").click(); });

function showDirectory(currentDirectory, addToHistory) {
    var directory_tree = currentDirectory.split("/");

    // Remove emtpy entries in the tree (this is necessary since a '/' at the
    // start or end will result in an empty string at that position.
    directory_tree = $.grep(directory_tree, function(s,i) { return s != "" } );

    return function(res) {
        // The following is to make the back and forward buttons to work
        // without reloading the page (only works on recent browsers).
        if ( addToHistory ) {
            window.history.pushState(currentDirectory, "Mosler", "{% url "horizon:mosler:files:index_path" "" %}" + currentDirectory );
        }

        $("#mosler-export-spinner").hide();
        $("#mosler-container").empty();

        // The directory path at the top
        var path_container = $("<ol/>", { 'class': 'mosler-path', });
        $("<li/>", { 'class': 'mosler-button' })
            .append('/')
            .appendTo(path_container)
            .click( _createDirClickFunction('') );
        for (var idx=0; idx < directory_tree.length; idx++) {
            var path_so_far = directory_tree.slice(0,idx+1).join("/");
            $("<li/>", { 'class': 'mosler-button' })
                .append(directory_tree[idx])
                .appendTo(path_container)
                .click( _createDirClickFunction( path_so_far ) );
        }
        path_container.appendTo($("#mosler-container"));

        // The contents of the current folder
        var contents = $('<ol/>', { 'class': 'mosler-file-list', });
        for (var idx=0; idx < res.length; idx++) {
            var listitem = $('<li>');
            var ahref    = $("<a/>");

            $("<img/>", { 'src': '/static/mosler/img/' + res[idx].type + '.png', })
                .appendTo(ahref);

            if ( res[idx].type == 'file' ) {
                ahref.attr({ 'href': '{% url "horizon:mosler:get_file" "" %}' + currentDirectory + "/" + res[idx].name, });
            }
            else {
                var d = currentDirectory + '/' + res[idx].name;
                d = d.replace(/^\//g, '');
                listitem.click( _createDirClickFunction(d) );
            }

            ahref.append(res[idx].name);
            listitem
                .append(ahref)
                .append(" ");
            $("<span/>", { 'class':'mosler-size' })
                .text(res[idx].size + res[idx].unit)
                .appendTo(listitem);
            listitem.appendTo(contents);
        }
        $("#mosler-container").append(contents);
    };
}

// This is needed because of JavaScript's strange scoping rules. Variables are
// shared between iterations of each loop, so we need to scope the targetDir
// variable inside a new funtion to get around this. See for example:
//  http://stackoverflow.com/questions/750486/javascript-closure-inside-loops-simple-practical-example
function _createDirClickFunction(targetDir) {
    return function() {
        // Show the spinner
        $("#mosler-export-spinner").show();

        // Update the directory listing
        listDir(targetDir, true)
    };
}

function listDir(directoryToList, addToHistory) {
    $.getJSON(
        "{% url "horizon:mosler:list_files" "" %}" + directoryToList,
        showDirectory(directoryToList, addToHistory)
    ).fail(function(jqxhr, textStatus, error) {
        $("#mosler-export-spinner").hide();
        var err = textStatus + ", " + error + ' :: ' + jqxhr.status + ' :: ' + jqxhr.responseText;
        console.log(err)
        if ( jqxhr.status == 500 ) {
            horizon.alert("error", "Can't contact the file server");
        }
        else {
            if ( jqxhr.responseText.length > 5 ) {
                horizon.alert("error", jqxhr.responseText);
            }
            else {
                horizon.alert("error", "Directory not found");
            }
        }
    });
}


/* This is the javascript that handles uploads */

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

// In this array we keep track of all files that are slated for uploading.
var files = [];

$(function() {
    var csrftoken = getCookie('csrftoken');
    $('#mosler-fileupload').fileupload({
        dataType: 'json',
        limitConcurrentUploads: 2,
        beforeSend: function(xhr, settings) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        },
        add: function(e, data) {
            var file_name = data.files[0].name;
            var new_row = $('<tr />', { "class": "mosler-upload"} );

            var spinner = $('<td />', {"class": "mosler-spinner" });
            spinner.appendTo(new_row);

            var progress_text = $('<td />', { "class": "mosler-progress-text" }).text('');
            progress_text.appendTo(new_row);

            $('<td />', { "class": "mosler-filename" } )
                .text(file_name)
                .appendTo(new_row);

            $('<td class="mosler-delete"><span class="fa fa-remove"></span></td>')
                .appendTo(new_row)
                .click(function(){
                    data.context['deleted'] = true;
                    new_row.remove();
                    if ( data.context['uploading'] ) {
                        data.context['uploading'] = false;
                        horizon.alert('warning', "Aborting upload!");
                        data.context['jqXHR'].abort();
                    }
                });

            new_row.prependTo($("#mosler-upload-progress"));

            data.context = {
                "text": progress_text,
                "spinner": spinner,
                "deleted": false,
                "uploading": false,
            };

            if ( data.files[0].size >= 1.99*Math.pow(2,30) ) {
                horizon.alert('error', "To upload files larger than 2Gb you need to use an SFTP program.");
                data.context['text'].empty()
                var err = $('<span class="mosler-error" />');
                err.text('Too big');
                err.appendTo( data.context['text'] );
                return;
            }
            files.push(data);

            $("#mosler-upload-button").removeAttr("disabled");
            $("#mosler-upload-button").removeClass('mosler-upload-button').addClass('mosler-button');
        },
        done: function (e, data) {
            data.context['uploading'] = false;
            data.context['text'].text('Done');
            data.context['spinner'].empty();
        },
        fail: function (e, data) {
            data.context['uploading'] = false;
            data.context['spinner'].empty(); // Remove spinner
            if ( ! data.context['deleted'] ) {
                data.context['text'].empty();

                horizon.alert("error", data.jqXHR.responseJSON.error);
                var err = $('<span class="mosler-error" />');
                err.text('Error');
                err.appendTo( data.context['text'] );
            }
        },
        progress: function(e, data) {
            data.context['text'].text('Uploading');
        },
    });
});

$("#mosler-upload-button").click(function() {
    $("#mosler-upload-button").removeClass('mosler-button').addClass('mosler-upload-button');
    $("#mosler-upload-button").attr("disabled","disabled");

    var copy = files;
    files = [];

    $.each(copy, function(key, data) {
        if ( ! data.context['deleted'] ) {
            var jqXHR = data.submit();
            data.context['jqXHR'] = jqXHR;
            data.context['uploading'] = true;
            data.context['text'].text('In queue');
            $('<img src="/static/dashboard/img/spinner.gif" />').appendTo(data.context['spinner']);
        }
    });
});

