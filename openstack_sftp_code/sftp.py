import base64
import socket # For exception importation

import paramiko
from paramiko.rsakey import RSAKey

import logging
LOG = logging.getLogger(__name__)


def _get_mosler_key():
    """The RSA key for the sftp server"""
    binary = base64.decodestring('AAAAB3NzaC1yc2EAAAADAQABAAABAQC36rThhzm4jeZFtXCbNhu/sArVRbDP50qhNJ5JsXB723UxXsE4g0/aOHcuezdIPl0KggHyRBX+gxFd3fkYmQW3ToBNEXlT/eWi3jL2L+4gqtAJI0pLiTNX/UmLxCoKjlAkWYIur+dqMDhcs73UE9vlG+zPCSZlJYxmrzWEKAJmhzUzb6Bjh0/npEUN1CaMylgRJ3dwQfRLTm4pmR4nl0CShgx2DOfntTJaQ7lVLngO7lhVSsj5V3qCWz4Y5Pay8QdPjz5Xf2gPbgVCsM2JuU7Lbkzc9pFZd5kzFQNM2Q20mUqiZBh9SeCioXDz17AOcYcBQhDW/kca8ncC3xb4Uhh7')
    return RSAKey(data=binary)

def get_client(username, password):
    """
    Given a request this method returns a tuple consisting of an sftp_client
    and an ssh_client, the ssh_client has to stick around until the sftp_client
    is done, otherwise the connection is closed (this bug is fixed in paramiko
    vcs but is not released in the version we are using).
    """
    client = paramiko.client.SSHClient()
    hostkeys = client.get_host_keys()
    host = 'filsluss.mosler.uppmax.uu.se'  # Production hostname
    #host = 'mosler.bils.se'                # Test environment hostname
    hostkeys.add(host, 'ssh-rsa', _get_mosler_key())

    LOG.debug("Connecting to sftp for %s" % (username))
    ## Check all the exceptions to generate nice error messages in the log
    try:
        client.connect(host, port=22, username=username, password=password,)
    except paramiko.BadHostKeyException as e:
        LOG.error("ssh host key validation error")
        raise e
    except paramiko.AuthenticationException as e:
        LOG.error("Could not authenticate user %s" % username)
        raise e
    except paramiko.SSHException as e:
        LOG.error("Got an SSH exception: %s" % e)
        raise e
    except socket.error as e:
        LOG.error("Socket error: %s" % e)
        raise e

    sftp_client = client.open_sftp()

    # The client closes the connection when it goes out of scope, so we need to
    # keep a reference to it. This is a bug that is fixed in upstream but
    # hasn't been released yet.
    return (sftp_client,client)
