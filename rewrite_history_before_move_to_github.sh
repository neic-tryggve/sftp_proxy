#!/bin/bash
# ------------------------------------------------------------------------
# This script removes things that are probably better left out from the
# repo when moving the project to the public, on github.
# It also renames some things that are named specific for tryggve, to
# somethingmore general.
# ------------------------------------------------------------------------
# Created: 2015-11-20
# Author: Samuel Lampa <samuel.lampa@bils.se>
# ------------------------------------------------------------------------
git filter-branch -f --prune-empty --tree-filter 'mv tryggve sftp_proxy' HEAD
git filter-branch -f --prune-empty --tree-filter 'mv sftp_proxy/tryggve sftp_proxy/webserver' HEAD
git filter-branch -f --prune-empty --tree-filter 'rm -rf openstack_sftp_code' HEAD
git filter-branch -f --prune-empty --tree-filter 'rm -rf meetings' HEAD
git filter-branch -f --prune-empty --tree-filter "find . -name '*.py' -exec sed -i -e 's/tryggve\ /webserver /g' {} \;"
git filter-branch -f --prune-empty --tree-filter "find . -name '*.py' -exec sed -i -e 's/tryggve\./webserver./g' {} \;"
git filter-branch -f --prune-empty --tree-filter 'mv sftp_proxy/webserver/settings.py sftp_proxy/webserver/settings.py.template' HEAD
git filter-branch -f --prune-empty --tree-filter "find . -name 'settings.py.template' -exec sed -i -e \"s/^SECRET_KEY.*$/SECRET_KEY = 'CHANGETHIS'/g\" {} \;"
git filter-branch -f --prune-empty --tree-filter "find . -name 'settings.py.template' -exec sed -i -e \"s/^ALLOWED_HOSTS.*$/ALLOWED_HOSTS = ['localhost']/g\" {} \;"
git filter-branch -f --prune-empty --tree-filter "find . -name '*.js' -exec sed -i -e 's/tsd\-fx01\.tsd\.usit\.no/host1.example.org/g' {} \;"
git filter-branch -f --prune-empty --tree-filter "find . -name '*.js' -exec sed -i -e 's/mosler\.bils\.se/host2.example.org/g' {} \;"
git filter-branch -f --prune-empty --tree-filter "find . -name '*.html' -exec sed -i -e 's/tsd\-fx01\.tsd\.usit\.no/host1.example.org/g' {} \;"
git filter-branch -f --prune-empty --tree-filter "find . -name '*.html' -exec sed -i -e 's/mosler\.bils\.se/host2.example.org/g' {} \;"
# Finally, delete this script too:
git filter-branch -f --prune-empty --tree-filter 'rm -rf rewrite_history_before_move_to_github.sh' HEAD
